package com.gitlab.khaledkassem.movies.data.network

import android.content.SharedPreferences
import androidx.lifecycle.MutableLiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.gitlab.khaledkassem.movies.common.Constants
import com.gitlab.khaledkassem.movies.common.extensions.getResponse
import com.gitlab.khaledkassem.movies.data.network.entities.ApiResponse
import com.gitlab.khaledkassem.movies.data.network.entities.MovieDetails
import com.gitlab.khaledkassem.movies.data.network.paging.PagingDataSourceFactory
import java.util.concurrent.Executors
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppNetworkHelper @Inject constructor(
    private val apiService: ApiService,
    private val pagedListConfig: PagedList.Config,
    private val preferences: SharedPreferences
) : NetworkHelper {

    private val popularMoviesDataSourceFactory by lazy {
        PagingDataSourceFactory(apiService::getNowPlaying)
    }

    override fun getNowPlayingApiResponse() =
        popularMoviesDataSourceFactory.getPagingApiResponse()

    override fun getNowPlayingMoviesList() =
        LivePagedListBuilder(popularMoviesDataSourceFactory, pagedListConfig)
            .setFetchExecutor(Executors.newFixedThreadPool(5))
            .build()


    override fun getMovieDetails(movieId: Int): MutableLiveData<ApiResponse<MovieDetails>> {

        val lang =
            if (preferences.getInt(Constants.CURRENT_LANGUAGE_KEY, 0) == Constants.ARABIC) "ar"
            else "en"

        return apiService.getMovieDetails(movieId, lang).getResponse()
    }

    override fun getMovieVideos(movieId: Int) =
        apiService.getMovieVideos(movieId).getResponse()

    override fun getMovieReviews(movieId: Int) =
        apiService.getMovieReviews(movieId).getResponse()
}