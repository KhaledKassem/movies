package com.gitlab.khaledkassem.movies.common.extensions

import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.annotation.Nullable
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.gitlab.khaledkassem.movies.BuildConfig
import com.gitlab.khaledkassem.movies.R
import com.gitlab.khaledkassem.movies.common.Constants
import com.gitlab.khaledkassem.movies.common.GlideApp


fun ImageView.loadImg(
    @Nullable photo: String,
    @DrawableRes placeHolder: Int = R.drawable.ic_vertical_place_holder,
    @DrawableRes error: Int = placeHolder
) {

    val myOptions = RequestOptions()
        .override(this.width, this.height)
        .fitCenter()

    GlideApp.with(context!!)
        .load("${BuildConfig.IMG_BASE_URL}w185$photo")
        .placeholder(placeHolder)
        .error(error)
        .fallback(error)
        .apply(myOptions)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(this)
}

fun ImageView.loadImgBanner(
    @Nullable photo: String,
    @DrawableRes placeHolder: Int = R.drawable.ic_place_holder,
    @DrawableRes error: Int = placeHolder
) {

    val myOptions = RequestOptions()
        .override(this.width, this.height)
        .fitCenter()

    GlideApp.with(context!!)
        .load("${BuildConfig.IMG_BASE_URL}w500$photo")
        .placeholder(placeHolder)
        .error(error)
        .fallback(error)
        .apply(myOptions)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(this)
}


fun ImageView.loadThumbnail(
    @Nullable video: String,
    @DrawableRes placeHolder: Int = R.drawable.ic_place_holder,
    @DrawableRes error: Int = placeHolder
) {

    val myOptions = RequestOptions()
        .override(this.width, this.height)
        .fitCenter()

    GlideApp.with(context!!)
        .load(Constants.YOUTUBE_THUMBNAIL_URL.replace("VIDEO_ID", video))
        .placeholder(placeHolder)
        .error(error)
        .fallback(error)
        .apply(myOptions)
        .diskCacheStrategy(DiskCacheStrategy.ALL)
        .into(this)
}
