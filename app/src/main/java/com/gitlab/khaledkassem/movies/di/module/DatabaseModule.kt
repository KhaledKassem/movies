package com.gitlab.khaledkassem.movies.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton
import androidx.room.Room
import com.gitlab.khaledkassem.movies.common.Constants
import com.gitlab.khaledkassem.movies.data.database.schema.MovieDatabase

/**
 * Module which provides all required dependencies about Room Database
 */
@Module
class DatabaseModule {


    /**
     * Provides the database object
     *
     * @param context the context
     * @return the MovieDatabase instance
     */
    @Provides
    @Singleton
    fun provideDatabase(context: Context) = Room
        .databaseBuilder(context, MovieDatabase::class.java, Constants.DATABASE_NAME)
        .fallbackToDestructiveMigration()
        .build()
}