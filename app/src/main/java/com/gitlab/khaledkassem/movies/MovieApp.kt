package com.gitlab.khaledkassem.movies

import android.app.Application
import com.gitlab.khaledkassem.movies.di.component.DaggerAppComponent
import com.gitlab.khaledkassem.movies.di.module.AppModule

// Movie app 
class MovieApp : Application() {

    val component by lazy {
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}