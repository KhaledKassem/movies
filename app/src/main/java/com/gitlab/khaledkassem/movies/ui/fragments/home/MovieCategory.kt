package com.gitlab.khaledkassem.movies.ui.fragments.home

import com.gitlab.khaledkassem.movies.ui.adapters.MoviesPagedAdapter

class MovieCategory (val moviesPagedAdapter: MoviesPagedAdapter)
