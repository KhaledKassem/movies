package com.gitlab.khaledkassem.movies.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.gitlab.khaledkassem.movies.common.extensions.goToActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        goToActivity(MainActivity::class.java)
        finish()
    }
}
