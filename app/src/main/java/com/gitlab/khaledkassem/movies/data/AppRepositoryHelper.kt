package com.gitlab.khaledkassem.movies.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.gitlab.khaledkassem.movies.data.database.AppDatabaseHelper
import com.gitlab.khaledkassem.movies.data.database.schema.entities.FavMovie
import com.gitlab.khaledkassem.movies.data.network.AppNetworkHelper
import com.gitlab.khaledkassem.movies.data.network.entities.ApiResponse
import com.gitlab.khaledkassem.movies.data.network.entities.Movie
import com.gitlab.khaledkassem.movies.data.preferences.AppPreferenceHelper
import okhttp3.ResponseBody
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppRepositoryHelper @Inject constructor(
    private val networkHelper: AppNetworkHelper,
    private val preferenceHelper : AppPreferenceHelper,
    private val databaseHelper: AppDatabaseHelper) : RepositoryHelper {

    /** networking **/
    override fun getNowPlayingMoviesList() = networkHelper.getNowPlayingMoviesList()

    override fun getNowPlayingApiResponse() = networkHelper.getNowPlayingApiResponse()


    override fun getMovieDetails(movieId: Int) = networkHelper.getMovieDetails(movieId)
    override fun getMovieVideos(movieId: Int) = networkHelper.getMovieVideos(movieId)
    override fun getMovieReviews(movieId: Int) = networkHelper.getMovieReviews(movieId)


    /** preferences **/
    override fun setUserLanguage(language: Int) = preferenceHelper.setUserLanguage(language)
    override fun getUserLanguage() = preferenceHelper.getUserLanguage()


    /** database **/
    override suspend fun insertFavMovie(favMovie: FavMovie) = databaseHelper.insertFavMovie(favMovie)
    override suspend fun deleteFavMovie(favMovie: FavMovie) = databaseHelper.deleteFavMovie(favMovie)
    override suspend fun getAllFavMovies() = databaseHelper.getAllFavMovies()
    override suspend fun getFavMovieById(movieId: Int) = databaseHelper.getFavMovieById(movieId)
}