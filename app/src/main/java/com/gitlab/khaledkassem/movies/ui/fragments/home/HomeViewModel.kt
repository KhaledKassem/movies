package com.gitlab.khaledkassem.movies.ui.fragments.home

import android.app.Application
import com.gitlab.khaledkassem.movies.common.Constants
import com.gitlab.khaledkassem.movies.ui.base.BaseViewModel

class HomeViewModel(app: Application) : BaseViewModel(app) {

    /**
     * list of categories to be displayed in the home fragment.
     * saved in viewModel to keep the instance of expanding.
     */
    var categories: List<MovieCategory>? = null

    /**
     * counter for the number of networking requests in parallel,
     * check its value to stop loading after finishing all requests.
     */
    private var numOfRequests = 0

    /**
     * change the value of language that's stored in shared pref.
     */
    fun changeLang() {

       if (appRepositoryHelper.getUserLanguage() == Constants.ARABIC)
            appRepositoryHelper.setUserLanguage(Constants.ENGLISH)
        else
           appRepositoryHelper.setUserLanguage(Constants.ARABIC)
    }

    /** get liveData of popular movies as pagedList **/
    fun getPopularMoviesList() = appRepositoryHelper.getNowPlayingMoviesList()

    /**
     * handle the response of popular movies request.
     */
    fun getPopularMoviesApiResponse() {

        numOfRequests++
        isLoading.value = true

        val popularMoviesApiResponse =
            appRepositoryHelper.getNowPlayingApiResponse()

        errorResponse.removeSource(popularMoviesApiResponse)
        errorResponse.addSource(popularMoviesApiResponse) {

            if (numOfRequests > 0)
                numOfRequests--

            if (numOfRequests == 0)
                isLoading.value = false

            errorResponse.value = it
        }
    }
}