package com.gitlab.khaledkassem.movies.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.gitlab.khaledkassem.movies.R
import com.gitlab.khaledkassem.movies.common.extensions.loadImg
import com.gitlab.khaledkassem.movies.data.network.entities.Movie
import com.gitlab.khaledkassem.movies.databinding.ItemMovieBinding
import com.gitlab.khaledkassem.movies.ui.base.BasePagedListAdapter
import com.gitlab.khaledkassem.movies.ui.base.BaseViewHolder
import com.gitlab.khaledkassem.movies.ui.fragments.home.HomeView

class MoviesPagedAdapter(private val homeView: HomeView) : BasePagedListAdapter() {


    override fun createBinding(parent: ViewGroup, viewType: Int) = DataBindingUtil.inflate(
        LayoutInflater.from(parent.context), R.layout.item_movie, parent, false
    ) as ItemMovieBinding

    override fun bind(binding: ViewDataBinding, position: Int) {

        val movie = getItem(position) as Movie.Result

        (binding as ItemMovieBinding).imgPoster.loadImg(movie.posterPath?:"")
        binding.root.setOnClickListener { homeView.setOnMovieSelected(movie.id, movie.title) }
    }


    inner class ItemMovieViewHolder(binding: ItemMovieBinding) :
        BaseViewHolder<ItemMovieBinding>(binding)
}