package com.gitlab.khaledkassem.movies.common.extensions

fun <T>List<T>.remove(position: Int)  = this.filter { it != this[position] }