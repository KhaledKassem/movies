package com.gitlab.khaledkassem.movies.di.component

import android.content.Context
import android.content.SharedPreferences
import com.gitlab.khaledkassem.movies.di.module.AppModule
import com.gitlab.khaledkassem.movies.di.module.NetworkModule
import com.gitlab.khaledkassem.movies.MovieApp
import com.gitlab.khaledkassem.movies.di.module.DatabaseModule
import com.gitlab.khaledkassem.movies.ui.base.BaseViewModel
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class, NetworkModule::class, DatabaseModule::class])
interface AppComponent {

    fun app(): MovieApp
    fun context(): Context

    fun inject(baseViewModel: BaseViewModel)

    fun getSharedPreference(): SharedPreferences
}