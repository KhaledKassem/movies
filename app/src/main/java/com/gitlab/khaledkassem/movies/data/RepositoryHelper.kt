package com.gitlab.khaledkassem.movies.data

import com.gitlab.khaledkassem.movies.data.database.DatabaseHelper
import com.gitlab.khaledkassem.movies.data.network.NetworkHelper
import com.gitlab.khaledkassem.movies.data.preferences.PreferenceHelper

interface RepositoryHelper : NetworkHelper, PreferenceHelper, DatabaseHelper