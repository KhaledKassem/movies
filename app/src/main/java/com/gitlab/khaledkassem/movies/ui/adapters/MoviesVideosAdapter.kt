package com.gitlab.khaledkassem.movies.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.gitlab.khaledkassem.movies.R
import com.gitlab.khaledkassem.movies.common.extensions.loadThumbnail
import com.gitlab.khaledkassem.movies.data.network.entities.MovieVideos
import com.gitlab.khaledkassem.movies.databinding.ItemVideoBinding
import com.gitlab.khaledkassem.movies.ui.base.BaseViewHolder
import com.gitlab.khaledkassem.movies.ui.fragments.movie_details.MovieDetailsView

class MoviesVideosAdapter(private val videos : List<MovieVideos.Result>,
                          private val movieDetailsView: MovieDetailsView) :
    RecyclerView.Adapter<MoviesVideosAdapter.ItemMovieVideoViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemMovieVideoViewHolder {
        val binding : ItemVideoBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_video,
            parent, false)

        return ItemMovieVideoViewHolder(binding)
    }

    override fun getItemCount() = videos.size

    override fun onBindViewHolder(holder: ItemMovieVideoViewHolder, position: Int) {

        holder.binding.imgVideoThumbnail.loadThumbnail(videos[position].key)

        holder.binding.root.setOnClickListener {
            movieDetailsView.onVideoClicked(videos[position].key)
        }
    }


    inner class ItemMovieVideoViewHolder(binding: ItemVideoBinding) :
        BaseViewHolder<ItemVideoBinding>(binding)
}