package com.gitlab.khaledkassem.movies.ui.fragments.home

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.gitlab.khaledkassem.movies.R
import com.gitlab.khaledkassem.movies.common.extensions.goToActivity
import com.gitlab.khaledkassem.movies.databinding.FragmentHomeBinding
import com.gitlab.khaledkassem.movies.ui.activities.SplashActivity
import com.gitlab.khaledkassem.movies.ui.adapters.MoviesCategoriesAdapter
import com.gitlab.khaledkassem.movies.ui.adapters.MoviesPagedAdapter
import com.gitlab.khaledkassem.movies.ui.base.BaseFragment

class HomeFragment : HomeView,
    BaseFragment<HomeViewModel, FragmentHomeBinding>(HomeViewModel::class.java) {

    private val popularMoviesAdapter by lazy { MoviesPagedAdapter(this) }

    override fun getLayoutRes() = R.layout.fragment_home

    override fun initViewModel(viewModel: HomeViewModel) {
        mBinding.viewModel = viewModel
    }


    override fun init(savedInstanceState: Bundle?) {
        initHomeToolbar()
        initCategories()
        getPopularMovies(savedInstanceState)
    }


    override fun initHomeToolbar() {

        mBinding.imgFav.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_favoritesFragment)
        }
    }

    override fun initCategories() {

        if (viewModel.categories.isNullOrEmpty())
            viewModel.categories = listOf(
                MovieCategory(popularMoviesAdapter))

        mBinding.recyclerCategories.layoutManager = LinearLayoutManager(context)
        mBinding.recyclerCategories.adapter =
            MoviesCategoriesAdapter(context!!, viewModel.categories!!)
    }

    override fun getPopularMovies(savedInstanceState: Bundle?) {

        if (popularMoviesAdapter.itemCount != 0) return

        viewModel.getPopularMoviesList().observe(this, Observer {
            popularMoviesAdapter.setList(it)
        })

        viewModel.getPopularMoviesApiResponse()
    }


    override fun setOnMovieSelected(movieId: Int, movieTitle: String) {
        val action =
            HomeFragmentDirections.actionHomeFragmentToMovieDetailsFragment(movieId, movieTitle)

        findNavController().navigate(action)
    }
}