package com.gitlab.khaledkassem.movies.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.gitlab.khaledkassem.movies.R
import com.gitlab.khaledkassem.movies.data.network.entities.MovieReviews
import com.gitlab.khaledkassem.movies.databinding.ItemMovieCategoryBinding
import com.gitlab.khaledkassem.movies.databinding.ItemReviewBinding
import com.gitlab.khaledkassem.movies.ui.base.BaseViewHolder
import com.gitlab.khaledkassem.movies.ui.fragments.movie_details.MovieDetailsView

class MoviesReviewsAdapter(private val reviews : List<MovieReviews.Result>,
                           private val movieDetailsView: MovieDetailsView) :
    RecyclerView.Adapter<MoviesReviewsAdapter.ItemMovieReviewViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemMovieReviewViewHolder {
        val binding : ItemReviewBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_review,
            parent, false)

        return ItemMovieReviewViewHolder(binding)
    }

    override fun getItemCount() = reviews.size

    override fun onBindViewHolder(holder: ItemMovieReviewViewHolder, position: Int) {

        holder.binding.tvAuthorName.text = reviews[position].author
        holder.binding.tvReviewContent.text = reviews[position].content

        holder.binding.root.setOnClickListener {
            movieDetailsView.onReviewClicked(reviews[position].url)
        }
    }


    inner class ItemMovieReviewViewHolder(binding: ItemReviewBinding) :
        BaseViewHolder<ItemReviewBinding>(binding)
}