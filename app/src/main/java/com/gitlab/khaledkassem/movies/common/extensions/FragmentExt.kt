package com.gitlab.khaledkassem.movies.common.extensions

import android.annotation.SuppressLint
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.gitlab.khaledkassem.movies.common.Constants
import com.google.android.material.snackbar.Snackbar


fun Fragment.getColorCompat(@ColorRes id: Int) = context!!.getColorCompat(id)

fun Fragment.goToActivity(activityClass: Class<*>) = activity?.goToActivity(activityClass)


@SuppressLint("WrongConstant")
fun Fragment.errorMsg(msg: String, duration: Int = Constants.SNAK_BAR_DURATION) {

    view?.run {
        val snackbar = Snackbar.make(this, msg, duration)
        snackbar.view.setBackgroundColor(getColorCompat(android.R.color.holo_red_dark))
        val textView = snackbar.view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        textView.setTextColor(getColorCompat(android.R.color.white))
        snackbar.show()
    }
}


fun Fragment.errorMsg(@StringRes msgId: Int, duration: Int = Constants.SNAK_BAR_DURATION) {
    errorMsg(getString(msgId), duration)
}
