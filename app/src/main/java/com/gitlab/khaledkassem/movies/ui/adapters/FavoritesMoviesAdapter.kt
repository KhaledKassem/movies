package com.gitlab.khaledkassem.movies.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.gitlab.khaledkassem.movies.R
import com.gitlab.khaledkassem.movies.common.extensions.loadImg
import com.gitlab.khaledkassem.movies.data.database.schema.entities.FavMovie
import com.gitlab.khaledkassem.movies.databinding.ItemFavoriteMovieBinding
import com.gitlab.khaledkassem.movies.databinding.ItemMovieCategoryBinding
import com.gitlab.khaledkassem.movies.databinding.ItemReviewBinding
import com.gitlab.khaledkassem.movies.ui.base.BaseViewHolder
import com.gitlab.khaledkassem.movies.ui.fragments.fav.FavoritesView

class FavoritesMoviesAdapter(private val favorites : List<FavMovie>,
                             private val favoritesView: FavoritesView) :
    RecyclerView.Adapter<FavoritesMoviesAdapter.ItemFavoriteMovie>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemFavoriteMovie {
        val binding : ItemFavoriteMovieBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_favorite_movie,
            parent, false)

        return ItemFavoriteMovie(binding)
    }

    override fun getItemCount() = favorites.size

    override fun onBindViewHolder(holder: ItemFavoriteMovie, position: Int) {

        holder.binding.imgPoster.loadImg(favorites[position].poster)
        holder.binding.tvMovieTitle.text = favorites[position].title
        holder.binding.tvMovieOverview.text = favorites[position].overview

        holder.binding.root.setOnClickListener {
            favoritesView.setOnMovieSelected(favorites[position].id, favorites[position].title)
        }
    }


    inner class ItemFavoriteMovie(binding: ItemFavoriteMovieBinding) :
        BaseViewHolder<ItemFavoriteMovieBinding>(binding)
}