package com.gitlab.khaledkassem.movies.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gitlab.khaledkassem.movies.R
import com.gitlab.khaledkassem.movies.databinding.ItemMovieCategoryBinding
import com.gitlab.khaledkassem.movies.ui.base.BaseViewHolder
import com.gitlab.khaledkassem.movies.ui.fragments.home.MovieCategory

class MoviesCategoriesAdapter(private val context: Context,
                              private val categories : List<MovieCategory>) :
    RecyclerView.Adapter<MoviesCategoriesAdapter.ItemMovieCategoryViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemMovieCategoryViewHolder {
        val binding : ItemMovieCategoryBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context), R.layout.item_movie_category,
            parent, false)

        return ItemMovieCategoryViewHolder(binding)
    }

    override fun getItemCount() = categories.size

    override fun onBindViewHolder(holder: ItemMovieCategoryViewHolder, position: Int) {

        holder.binding.recyclerMovies.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        holder.binding.recyclerMovies.adapter = categories[position].moviesPagedAdapter

    }


    inner class ItemMovieCategoryViewHolder(binding: ItemMovieCategoryBinding) :
        BaseViewHolder<ItemMovieCategoryBinding>(binding)
}