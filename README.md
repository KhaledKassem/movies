Movies App :
  1- Wrote by Kotlin language 
  2- Use Dagger2 for DI 
  3- Room for local storage 
  4- MVVM Architecture design pattern 

**(Snapshot for Movie app)**
(https://drive.google.com/file/d/16DRL--EKhp7CPkEppiDZ1FzEhFMZoiPj/view?usp=sharing)
(https://drive.google.com/file/d/13qDgWPz94yUg0xddg19iDGi5_whkpQbu/view?usp=sharing)
(https://drive.google.com/file/d/1Pju79x8rwApgahO6u7zj4a7bmc52-ie2/view?usp=sharing)